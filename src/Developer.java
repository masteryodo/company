public class Developer extends Employee{

    Developer(String name, double salary, String position) {
        super(name, salary, position);
    }

    @Override
    public void doWork() {
        programm();
    }

    public void programm(){
        System.out.println(" программирую программирую программирую");
    }
}

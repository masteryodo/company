public class Boss extends Employee {

    Boss(String name, double salary, String position) {
        super(name, salary, position);
    }

    @Override
    public void doWork() {
        yell();
        manage();
    }

    public void yell(){
        System.out.println("Всех уволю !!!");
    }

    public void manage(){
        System.out.println("Всем работать !!!");
    }
}

public class Janitor extends Employee{


    public Janitor(String name, double salary, String position) {
        super(name, salary, position);
    }

    @Override
    public void doWork() {
        cleanUp();
    }

    public void cleanUp(){
        System.out.println(" убираюсь ");
    }
}

public class Main{
    public static void main(String[] args) {
        Company company = new Company();

        company.employees[0] = new Boss("Vasya", 10000.00, "BigBoss");
        company.employees[1] = new Developer("Vasya", 10000.00, "BigBoss");
        company.employees[2] = new Developer("Vasya", 10000.00, "BigBoss");
        company.employees[3] = new Developer("Vasya", 10000.00, "BigBoss");
        company.employees[4] = new Janitor("Vasya", 10000.00, "BigBoss");
        company.employees[5] = new Janitor("Vasya", 10000.00, "BigBoss");

        for (int i = 0; i < company.employees.length; i++) {
            company.employees[i].doWork();
        }

    }
}